package com.showmeyourcode.vertx.demo.verticle;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import com.showmeyourcode.vertx.demo.BaseRestIntegration;
import com.showmeyourcode.vertx.demo.constant.RouterPathConstant;

class HealthVerticleIT extends BaseRestIntegration {

	@Test
	@DisplayName("GET /health EXPECTED 200 status code")
	void shouldGetHealthStatus(Vertx vertx, VertxTestContext testContext) {
		webClient.get(port, LOCALHOST, RouterPathConstant.HEALTH).as(BodyCodec.jsonObject())
				.send(testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(
							() -> Assertions.assertEquals(HttpResponseStatus.OK.code(), response.statusCode()),
							() -> Assertions.assertEquals(
									readFileAsJsonObject("src/test/resources/responses/health.json"),
									response.body())));
					testContext.completeNow();
				}));
	}

}

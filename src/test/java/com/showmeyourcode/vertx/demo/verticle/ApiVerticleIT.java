package com.showmeyourcode.vertx.demo.verticle;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Vertx;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.junit5.VertxTestContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import com.showmeyourcode.vertx.demo.BaseRestIntegration;
import com.showmeyourcode.vertx.demo.constant.RouterPathConstant;
import com.showmeyourcode.vertx.demo.model.GetAllNotesDTO;
import com.showmeyourcode.vertx.demo.model.NoteDTO;
import com.showmeyourcode.vertx.demo.model.NoteCreationDTO;

import java.io.IOException;
import java.time.OffsetDateTime;

// Junit5 parameterized tests integration - https://github.com/eclipse-vertx/vertx-junit5/pull/83/files
@Slf4j
class ApiVerticleIT extends BaseRestIntegration {

	@Test
	@DisplayName("GET /api/v1/notes without query params EXPECTED 200 status code")
	void shouldGetAllNotes(Vertx vertx, VertxTestContext testContext) {
		CompositeFuture.all(
				repository.save(
						new NoteCreationDTO("Example author", OffsetDateTime.now().toString(), "Example content1")),
				repository.save(
						new NoteCreationDTO("Example author", OffsetDateTime.now().toString(), "Example content2")),
				repository.save(
						new NoteCreationDTO("Example author", OffsetDateTime.now().toString(), "Example content3")))
				.onSuccess(r -> {
					webClient.get(port, LOCALHOST, RouterPathConstant.V1_NOTES).as(BodyCodec.jsonObject())
							.send(testContext.succeeding(response -> {
								testContext.verify(() -> Assertions.assertAll(() -> Assertions
										.assertEquals(HttpResponseStatus.OK.code(), response.statusCode()), () -> {
											var notes = objectMapper.readValue(response.body().toString(),
													GetAllNotesDTO.class);
											Assertions.assertEquals(3, notes.getNotes().size());
											Assertions.assertEquals(0, notes.getPage());
											Assertions.assertEquals(10, notes.getLimit());
										}));

								testContext.completeNow();
							}));
				});
	}

	@Test
	@DisplayName("GET /api/v1/notes using query params EXPECTED 200 status code")
	void shouldGetEmptyNotesWhenPageNumberParamIsTooHigh(Vertx vertx, VertxTestContext testContext) {
		webClient.get(port, LOCALHOST, RouterPathConstant.V1_NOTES + "?l=10&pn=6").as(BodyCodec.jsonObject())
				.send(testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(
							() -> Assertions.assertEquals(HttpResponseStatus.OK.code(), response.statusCode()),
							() -> Assertions.assertEquals(
									readFileAsJsonObject("src/test/resources/responses/get-notes-empty.json"),
									response.body())));

					testContext.completeNow();
				}));
	}

	@ParameterizedTest
	@DisplayName("GET /api/v1/notes using invalid query params EXPECTED 400 status code")
	@ValueSource(strings = {
    "?l=abc&pn=rew",
    "?l=1ac2&pn=00cc1",
    "?l=12&pn=-1",
    "?l=-12&pn=-11",
    "?l=8&pn=1",
    "?l=10&pn=-1"
  })
	void shouldGetAllNotesWithBadParams(String queryParam, Vertx vertx, VertxTestContext testContext) {
		webClient.get(port, LOCALHOST, RouterPathConstant.V1_NOTES + queryParam).as(BodyCodec.jsonObject())
				.send(testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(() -> Assertions
							.assertEquals(HttpResponseStatus.BAD_REQUEST.code(), response.statusCode())));

					testContext.completeNow();
				}));
	}

	@ParameterizedTest
	@DisplayName("GET /api/v1/notes using invalid page query param EXPECTED 400 status code")
	@ValueSource(strings = {"?pn=00cc1", "?pn=x", "?pn=-1", "?pn=-12", "?pn="})
	void shouldGetAllNotesWithBadPageNumberParam(String queryParam, Vertx vertx, VertxTestContext testContext) {
		webClient.get(port, LOCALHOST, RouterPathConstant.V1_NOTES + queryParam).as(BodyCodec.jsonObject())
				.send(testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(() -> Assertions
							.assertEquals(HttpResponseStatus.BAD_REQUEST.code(), response.statusCode())));

					testContext.completeNow();
				}));
	}

	@ParameterizedTest
	@DisplayName("GET /api/v1/notes using valid page query param EXPECTED 200 status code")
	@ValueSource(strings = {"?pn=0", "?pn=1", "?pn=2", "?pn=12", "?pn=100"})
	void shouldGetAllNotesWithValidPageNumberParam(String queryParam, Vertx vertx, VertxTestContext testContext) {
		webClient.get(port, LOCALHOST, RouterPathConstant.V1_NOTES + queryParam).as(BodyCodec.jsonObject())
				.send(testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(
							() -> Assertions.assertEquals(HttpResponseStatus.OK.code(), response.statusCode())));

					testContext.completeNow();
				}));
	}

	@ParameterizedTest
	@DisplayName("GET /api/v1/notes using invalid page limit params EXPECTED 400 status code")
	@ValueSource(strings = {"?l=00cc1", "?l=x", "?l=-1", "?l=-12", "?l=", "?l=0", "?l=60", "?l=80", "?l=100"})
	void shouldGetAllNotesWithBadLimitParam(String queryParam, Vertx vertx, VertxTestContext testContext) {
		webClient.get(port, LOCALHOST, RouterPathConstant.V1_NOTES + queryParam).as(BodyCodec.jsonObject())
				.send(testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(() -> Assertions
							.assertEquals(HttpResponseStatus.BAD_REQUEST.code(), response.statusCode())));

					testContext.completeNow();
				}));
	}

	@ParameterizedTest
	@DisplayName("GET /api/v1/notes using valid page limit param EXPECTED 200 status code")
	@ValueSource(strings = {"?l=10", "?l=20", "?l=30", "?l=40", "?l=50"})
	void shouldGetAllNotesWithValidLimitParam(String queryParam, Vertx vertx, VertxTestContext testContext) {
		webClient.get(port, LOCALHOST, RouterPathConstant.V1_NOTES + queryParam).as(BodyCodec.jsonObject())
				.send(testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(
							() -> Assertions.assertEquals(HttpResponseStatus.OK.code(), response.statusCode())));

					testContext.completeNow();
				}));
	}

	@Test
	@DisplayName("POST /api/v1/notes with mandatory fields EXPECTED 201 status code")
	void shouldSaveNoteWithMandatoryFieldsSuccessfully(Vertx vertx, VertxTestContext testContext)
			throws IOException {
		var body = readFileAsJsonObject("src/test/resources/requests/create-note-only-mandatory-params.json");
		webClient.post(port, LOCALHOST, RouterPathConstant.V1_NOTES).putHeader("content-type", "application/json")
				.sendJsonObject(body, testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(
							() -> Assertions.assertEquals(HttpResponseStatus.CREATED.code(), response.statusCode()),
							() -> {
								var responseDTO = objectMapper.readValue(response.body().toString(),
										NoteDTO.class);
								Assertions.assertNotNull(responseDTO.getId());
								Assertions.assertNull(responseDTO.getAuthor());
								Assertions.assertNotNull(responseDTO.getContent());
								Assertions.assertNotNull(responseDTO.getCreation_date());
							}));

					testContext.completeNow();
				}));
	}

	@Test
	@DisplayName("POST /api/v1/notes with all fields EXPECTED 201 status code")
	void shouldSaveNoteWithAllFieldsSuccessfully(Vertx vertx, VertxTestContext testContext) throws IOException {
		var body = readFileAsJsonObject("src/test/resources/requests/create-note-all-fields.json");
		webClient.post(port, LOCALHOST, RouterPathConstant.V1_NOTES).putHeader("content-type", "application/json")
				.sendJsonObject(body, testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(
							() -> Assertions.assertEquals(HttpResponseStatus.CREATED.code(), response.statusCode()),
							() -> {
								var responseDTO = objectMapper.readValue(response.body().toString(),
										NoteDTO.class);
								Assertions.assertNotNull(responseDTO.getId());
								Assertions.assertNotNull(responseDTO.getAuthor());
								Assertions.assertNotNull(responseDTO.getContent());
								Assertions.assertNotNull(responseDTO.getCreation_date());
							}));

					testContext.completeNow();
				}));
	}

	@ParameterizedTest
	@DisplayName("POST /api/v1/notes using invalid payload EXPECTED 400 status code")
	@ValueSource(strings = {"create-note-too-long-author.json", "create-note-too-long-content.json",
			"create-note-too-short-author.json", "create-note-too-short-content.json",})
	void shouldNotCreateNoteWhenPayloadIsInvalid(String filePath, Vertx vertx, VertxTestContext testContext)
			throws IOException {
		var body = readFileAsJsonObject("src/test/resources/requests-invalid/" + filePath);
		webClient.post(port, LOCALHOST, RouterPathConstant.V1_NOTES).putHeader("content-type", "application/json")
				.sendJsonObject(body, testContext.succeeding(response -> {
					testContext.verify(() -> Assertions.assertAll(() -> Assertions
							.assertEquals(HttpResponseStatus.BAD_REQUEST.code(), response.statusCode())));

					testContext.completeNow();
				}));
	}
}

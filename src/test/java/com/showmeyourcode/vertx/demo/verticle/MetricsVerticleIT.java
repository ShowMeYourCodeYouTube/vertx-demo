package com.showmeyourcode.vertx.demo.verticle;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import com.showmeyourcode.vertx.demo.config.ApplicationProperties;
import com.showmeyourcode.vertx.demo.constant.RouterPathConstant;

import java.util.concurrent.ThreadLocalRandom;

import static com.showmeyourcode.vertx.demo.BaseRestIntegration.LOCALHOST;

/**
 *
 * Vert.x provides a nifty junit5 extension (VertxExtension) which automatically
 * injects the Vertx and VertxTestContext instances in your test methods. The
 * downside is that there is no way to configure the injected Vertx instance as
 * it always injects it with default settings.
 *
 * Ref:
 * https://stackoverflow.com/questions/54479117/how-to-configure-the-vertx-instance-itself-only-once-and-use-it-in-application-c
 *
 * Change proposal: https://github.com/eclipse-vertx/vert.x/issues/3514
 */
@Slf4j
@ExtendWith(VertxExtension.class)
class MetricsVerticleIT {

	protected final int SERVER_PORT = ThreadLocalRandom.current().nextInt(7000, 8000);

	@Test
	@DisplayName("GET /metrics EXPECTED 200 status code")
	void shouldGetPrometheusMetrics(VertxTestContext testContext) {
		var vertx = Vertx.vertx(MetricsVerticle.getVertxOptionsWithMetrics());
		var router = Router.router(vertx);

		vertx.deployVerticle(new MetricsVerticle(router)).onSuccess(event -> {
			log.debug("The verticle deployed successfully. Port: {}", SERVER_PORT);
		}).onFailure(throwable -> {
			log.error("Cannot deploy the verticle! ", throwable);
			System.exit(-1);
		});

		vertx.createHttpServer().requestHandler(router).listen(SERVER_PORT)
				.onComplete(testContext.succeeding(httpServer -> {
					log.info("HTTP server ready to accept requests.");
					WebClient.create(vertx).get(SERVER_PORT, LOCALHOST, RouterPathConstant.METRICS)
							.as(BodyCodec.string()).send(testContext.succeeding(response -> {
								testContext.verify(() -> Assertions.assertAll(
										() -> Assertions.assertEquals(HttpResponseStatus.OK.code(),
												response.statusCode()),
										() -> Assertions.assertNotNull(response.body()),
										() -> verifyIfResponseContainsMetrics(response.body())));

								testContext.completeNow();
							}));
				}));
	}

	private void verifyIfResponseContainsMetrics(String body) {
		Assertions.assertTrue(body.contains("process_cpu_usage"), "Should contain CPU usage");
		Assertions.assertTrue(body.contains("jvm_memory_used_bytes"), "Should contain JVM memory");
		Assertions.assertTrue(body.contains("vertx_http_client_queue_time_seconds_max"), "Should contain Vert.x stats");
		Assertions.assertTrue(body.contains("vertx_http_client_requests_total"), "Should contain Vert.x stats");
	}
}

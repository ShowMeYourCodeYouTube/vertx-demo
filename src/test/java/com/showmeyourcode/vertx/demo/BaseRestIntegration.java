package com.showmeyourcode.vertx.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.showmeyourcode.vertx.demo.verticle.MainVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import com.showmeyourcode.vertx.demo.config.ApplicationProperties;
import com.showmeyourcode.vertx.demo.config.JacksonConfig;
import com.showmeyourcode.vertx.demo.repository.DatabaseClient;
import com.showmeyourcode.vertx.demo.repository.NotesRepository;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(VertxExtension.class)
public abstract class BaseRestIntegration {

	public static final String LOCALHOST = "localhost";
	public static final ApplicationProperties properties = new ApplicationProperties();
	protected static String verticleDeploymentId;

	protected final int port = properties.getServerPort();
	protected WebClient webClient;
	protected ObjectMapper objectMapper;
	protected DatabaseClient databaseClient;
	protected NotesRepository repository;

	@BeforeAll
	static void setup(Vertx vertx, VertxTestContext testContext) {
		log.debug("BeforeAll: ");
		vertx.deployVerticle(new MainVerticle()).onSuccess(deploymentId -> {
			verticleDeploymentId = deploymentId;
			log.info("The app started ({})! Starting tests...", verticleDeploymentId);
			testContext.completeNow();
		}).onFailure(err -> {
			log.error("Cannot start the app! Message: {}", err.getMessage());
			testContext.failNow(err);
		});
	}

	@AfterAll
	static void afterAll(Vertx vertx, VertxTestContext testContext) {
		log.debug("AfterAll ({}): ", verticleDeploymentId);
		vertx.undeploy(verticleDeploymentId).onComplete(r -> {
			testContext.completeNow();
		});
	}

	@BeforeEach
	protected void beforeEach(Vertx vertx, VertxTestContext testContext) {
		webClient = WebClient.create(vertx);
		databaseClient = new DatabaseClient(vertx, properties);
		repository = new NotesRepository(databaseClient.getJdbcPool(), objectMapper);
		objectMapper = JacksonConfig.getObjectMapper();

		repository.deleteAll().onSuccess(r -> {
			log.debug("Database is empty and ready for tests.");
			testContext.completeNow();
		}).onFailure(testContext::failNow);
	}

	protected JsonObject readFileAsJsonObject(String path) throws IOException {
		return new JsonObject(Files.lines(Paths.get(path), StandardCharsets.UTF_8).collect(Collectors.joining("\n")));
	}
}

package com.showmeyourcode.vertx.demo.logging;

public enum LogMessage {
	RUN_HTTP_SERVER_SUCCESS_MESSAGE("HTTP server running on port %s"),
  NULL_OFFSET_ERROR_MESSAGE("Offset can't be null. Page %s and limit %s"),
  STARTING_MESSAGE("Starting %s..."),
  RUN_DURATION("Starting operation took %d ms");

	private final String message;

	LogMessage(final String message) {
		this.message = message;
	}

	public String buildMessage(Object... argument) {
		return String.format(message, argument);
	}
}

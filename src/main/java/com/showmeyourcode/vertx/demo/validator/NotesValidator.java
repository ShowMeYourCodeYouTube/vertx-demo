package com.showmeyourcode.vertx.demo.validator;

import io.vertx.core.Vertx;
import io.vertx.ext.web.validation.RequestPredicate;
import io.vertx.ext.web.validation.ValidationHandler;
import io.vertx.ext.web.validation.builder.Bodies;
import io.vertx.ext.web.validation.builder.ParameterProcessorFactory;
import io.vertx.ext.web.validation.builder.Parameters;
import io.vertx.json.schema.SchemaParser;
import io.vertx.json.schema.SchemaRouter;
import io.vertx.json.schema.SchemaRouterOptions;
import io.vertx.json.schema.common.dsl.ObjectSchemaBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.constant.ApiConstant;

import static io.vertx.json.schema.common.dsl.Keywords.maxLength;
import static io.vertx.json.schema.common.dsl.Keywords.minLength;
import static io.vertx.json.schema.common.dsl.Schemas.intSchema;
import static io.vertx.json.schema.common.dsl.Schemas.objectSchema;
import static io.vertx.json.schema.common.dsl.Schemas.stringSchema;
import static io.vertx.json.schema.draft7.dsl.Keywords.maximum;
import static io.vertx.json.schema.draft7.dsl.Keywords.minimum;

/**
 * Validation service using JSON schema validation.
 */
@Slf4j
@RequiredArgsConstructor
public class NotesValidator {

	private final Vertx vertx;

	public ValidationHandler validateGetRequest() {
		log.info("Setting validation for get notes requests.");
		final SchemaParser schemaParser = createSchemaParser();

		return ValidationHandler.builder(schemaParser).queryParameter(buildPageQueryParameter())
				.queryParameter(buildLimitQueryParameter()).build();
	}

	public ValidationHandler validatePostRequest() {
		log.info("Setting validation for a new note creation requests.");
		final SchemaParser schemaParser = createSchemaParser();
		final ObjectSchemaBuilder schemaBuilder = getBodySchemaBuilderForPostRequest();

		return ValidationHandler.builder(schemaParser).predicate(RequestPredicate.BODY_REQUIRED)
				.body(Bodies.json(schemaBuilder)).build();
	}

	private SchemaParser createSchemaParser() {
		return SchemaParser.createDraft7SchemaParser(SchemaRouter.create(vertx, new SchemaRouterOptions()));
	}

	private ObjectSchemaBuilder getBodySchemaBuilderForPostRequest() {
		return objectSchema().optionalProperty("author", stringSchema().with(minLength(2)).with(maxLength(50)))
				.requiredProperty("creation_date", stringSchema().with())
				.requiredProperty("content", stringSchema().with(minLength(3)).with(maxLength(600)));
	}

	private ParameterProcessorFactory buildPageQueryParameter() {
		return Parameters.optionalParam(ApiConstant.PAGE_NUMBER_QUERY_PARAM, intSchema().with(minimum(0)));
	}

	private ParameterProcessorFactory buildLimitQueryParameter() {
		return Parameters.optionalParam(ApiConstant.LIMIT_QUERY_PARAM, intSchema().with(maximum(50)).with(minimum(10)));
	}
}

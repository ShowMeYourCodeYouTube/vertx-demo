package com.showmeyourcode.vertx.demo.repository;

import io.vertx.core.Vertx;
import io.vertx.jdbcclient.JDBCConnectOptions;
import io.vertx.jdbcclient.JDBCPool;
import io.vertx.sqlclient.PoolOptions;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.config.ApplicationProperties;

@Slf4j
@Getter
public class DatabaseClient {

	private JDBCPool jdbcPool;

	public DatabaseClient(Vertx vertx, ApplicationProperties properties) {
		buildConnectionPool(vertx, properties);
	}

	public void buildConnectionPool(Vertx vertx, ApplicationProperties properties) {
		log.info("Initializing the database connection.");

    jdbcPool = JDBCPool.pool(
      vertx,
      new JDBCConnectOptions()
        .setJdbcUrl(properties.getJdbc())
        .setSchema(properties.getDatabaseSchema())
        .setUser(properties.getDatabaseUsername())
        .setPassword(properties.getDatabasePassword()),
      new PoolOptions()
        .setMaxSize(4)
        .setName("my-pool-name")
    );
  }
}

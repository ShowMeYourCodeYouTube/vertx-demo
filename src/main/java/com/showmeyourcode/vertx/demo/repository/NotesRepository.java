package com.showmeyourcode.vertx.demo.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.Future;
import io.vertx.core.impl.future.PromiseImpl;
import io.vertx.core.impl.future.SucceededFuture;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.Tuple;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.model.GetAllNotesDTO;
import com.showmeyourcode.vertx.demo.model.NoteDTO;
import com.showmeyourcode.vertx.demo.model.NoteCreationDTO;

import java.util.Iterator;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class NotesRepository {

	private static final String SQL_SELECT_ALL = "SELECT * FROM notes.note LIMIT $1 OFFSET $2";
	private static final String SQL_COUNT = "SELECT COUNT(*) AS total FROM notes.note";
	private static final String SQL_DELETE_ALL = "DELETE FROM notes.note";
	private static final String SQL_INSERT = "INSERT INTO notes.note (id, creation_date, content, author) VALUES ($1, $2, $3, $4)";
	private final SqlClient sqlClient;
	private final ObjectMapper objectMapper;

	public Future<GetAllNotesDTO> selectAll(int limit, int page, int offset) {
		var result = new PromiseImpl<GetAllNotesDTO>();
		sqlClient.preparedQuery(SQL_SELECT_ALL).execute(Tuple.of(limit, offset), ar -> {
			if (ar.succeeded()) {
				GetAllNotesDTO notes = new GetAllNotesDTO();
				notes.setLimit(limit);
				notes.setPage(page);
				Iterator<Row> iterator = ar.result().iterator();
				while (iterator.hasNext()) {
					var rowAsString = iterator.next().toJson().toString();
					try {
						notes.getNotes().add(objectMapper.readValue(rowAsString, NoteDTO.class));
					} catch (JsonProcessingException e) {
						log.error("Cannot map database result to entities! Result: {} ", rowAsString, e);
					}
				}
				result.complete(notes);
			} else {
				result.fail(ar.cause());
			}
		});
		return result;
	}

	public Future<Integer> count() {
		return sqlClient.preparedQuery(SQL_COUNT).execute().map(rows -> {
			int total = rows.iterator().next().getInteger("total");
			log.debug("Count query executed. Result: {}", total);
			return total;
		});
	}

	public Future<NoteDTO> save(NoteCreationDTO dto) {
		var result = new PromiseImpl<NoteDTO>();
    var id = UUID.randomUUID();
		sqlClient.preparedQuery(SQL_INSERT).execute(Tuple.of(id,dto.getCreation_date(), dto.getContent(), dto.getAuthor()),
				ar -> {
					if (ar.succeeded()) {
						log.debug("Updated no. of rows: {}", ar.result());
						result.onSuccess(
              new NoteDTO(
                id,
								dto.getAuthor(),
                dto.getCreation_date(),
                dto.getContent()
              )
            );
					} else {
						result.fail(ar.cause());
					}
				});
		return result;
	}

	public Future<Boolean> deleteAll() {
		return sqlClient.preparedQuery(SQL_DELETE_ALL).execute().flatMap(rows -> new SucceededFuture(true));
	}
}

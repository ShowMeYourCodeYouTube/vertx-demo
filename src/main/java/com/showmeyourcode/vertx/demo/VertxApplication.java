package com.showmeyourcode.vertx.demo;

import com.showmeyourcode.vertx.demo.verticle.MainVerticle;
import com.showmeyourcode.vertx.demo.verticle.MetricsVerticle;
import io.vertx.core.Vertx;
import lombok.extern.slf4j.Slf4j;

/**
 * A simple runner to start the app locally.
 * Instead of running the main method, you can use a Gradle command to start Vert.x.
 */
@Slf4j
public class VertxApplication {

  public static void main(String[] args) {
    log.info("Starting the Vert.x Notes app using {}", MainVerticle.class.getName());

    final Vertx vertx = Vertx.vertx(MetricsVerticle.getVertxOptionsWithMetrics());

    vertx.deployVerticle(new MainVerticle()).onFailure(throwable -> {
      log.error("Cannot deploy the Main Verticle! ", throwable);
      System.exit(-1);
    });
  }
}

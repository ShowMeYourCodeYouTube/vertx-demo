package com.showmeyourcode.vertx.demo.config;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

@Slf4j
public class ApplicationProperties {

	public static final String APP_PROFILE_ENV_VARIABLE = "NOTES_APP_PROFILE";

	public static final String DEFAULT_APPLICATION_PROPERTIES = "application.properties";

	public static final String DB_JDBC_CONFIG = "datasource.jdbc";
	public static final String DB_USERNAME_CONFIG = "datasource.username";
	public static final String DB_PASSWORD_CONFIG = "datasource.password";
	public static final String DB_SCHEMA_CONFIG = "datasource.schema";
	public static final String SERVER_PORT_CONFIG = "server.port";

	private final Properties properties;

	public ApplicationProperties() {
		this.properties = readProperties();
	}

	public Optional<Integer> getPropertyAsInt(String key) {
		return Optional.ofNullable(properties.getProperty(key)).map(Integer::parseInt);
	}

	private Properties readProperties() {
		Properties properties = new Properties();
		String propertiesFile = determineApplicationProperties();

		try {
			try (InputStream is = getClass().getClassLoader().getResourceAsStream(propertiesFile)) {
				try {
					properties.load(is);
				} catch (IOException e) {
					log.error("Cannot read properties! ", e);
				}
			}
		} catch (IOException e) {
			log.error("Cannot read properties! ", e);
		}

		return properties;
	}

	private String determineApplicationProperties() {
		var profile = System.getenv(APP_PROFILE_ENV_VARIABLE);
		if (profile != null) {
			return String.format("application-%s.properties", profile);
		} else {
			return DEFAULT_APPLICATION_PROPERTIES;
		}
	}

	public String getJdbc() {
		return properties.getProperty(ApplicationProperties.DB_JDBC_CONFIG);
	}

	public Integer getServerPort() {
		return getPropertyAsInt(ApplicationProperties.SERVER_PORT_CONFIG)
				.orElse(8080);
	}

	public String getDatabaseUsername() {
		return properties.getProperty(ApplicationProperties.DB_USERNAME_CONFIG);
	}

	public String getDatabasePassword() {
		return properties.getProperty(ApplicationProperties.DB_PASSWORD_CONFIG);
	}

	public String getDatabaseSchema() {
		return properties.getProperty(ApplicationProperties.DB_SCHEMA_CONFIG);
	}
}

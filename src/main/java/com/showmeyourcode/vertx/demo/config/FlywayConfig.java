package com.showmeyourcode.vertx.demo.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.api.configuration.Configuration;
import org.flywaydb.core.api.configuration.FluentConfiguration;

@Slf4j
@RequiredArgsConstructor
public class FlywayConfig {

	private final ApplicationProperties properties;

	public Configuration buildFlywayConfiguration() {
		final String url = properties.getJdbc();
		log.info("DB connection string: {}", url);

		return new FluentConfiguration()
      .dataSource(
          url,
          properties.getDatabaseUsername(),
          properties.getDatabasePassword()
      )
      .createSchemas(true)
      .schemas(properties.getDatabaseSchema())
      .defaultSchema(properties.getDatabaseSchema());
	}
}

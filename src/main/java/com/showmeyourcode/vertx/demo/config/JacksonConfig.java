package com.showmeyourcode.vertx.demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.vertx.core.json.jackson.DatabindCodec;

public class JacksonConfig {

	private JacksonConfig() {
	}

	/**
	 * Configure th default ObjectMapper initialized by Vert.x
	 */
	public static void configureJackson() {
		ObjectMapper mapper = getObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.setPropertyNamingStrategy(PropertyNamingStrategies.LOWER_CAMEL_CASE);

		ObjectMapper prettyMapper = DatabindCodec.prettyMapper();
		prettyMapper.registerModule(new JavaTimeModule());

		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	}

	public static ObjectMapper getObjectMapper() {
		return DatabindCodec.mapper();
	}
}

package com.showmeyourcode.vertx.demo.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.config.ApplicationProperties;
import com.showmeyourcode.vertx.demo.config.FlywayConfig;
import com.showmeyourcode.vertx.demo.config.JacksonConfig;
import com.showmeyourcode.vertx.demo.logging.LogMessage;
import com.showmeyourcode.vertx.demo.repository.DatabaseClient;
import com.showmeyourcode.vertx.demo.repository.NotesRepository;
import com.showmeyourcode.vertx.demo.verticle.NotesServiceVerticle;
import com.showmeyourcode.vertx.demo.validator.NotesValidator;
import com.showmeyourcode.vertx.demo.verticle.ApiVerticle;
import com.showmeyourcode.vertx.demo.verticle.FlywayVerticle;
import com.showmeyourcode.vertx.demo.verticle.HealthVerticle;
import com.showmeyourcode.vertx.demo.verticle.MetricsVerticle;

@Slf4j
public class MainVerticle extends AbstractVerticle {

  private ApplicationProperties properties;
  private DatabaseClient databaseClient;
  private NotesRepository repository;
  private NotesValidator notesValidator;

  @Override
  public void start(Promise<Void> startPromise) {
    log.info(LogMessage.STARTING_MESSAGE.buildMessage(this.getClass().getName()));
    final long start = System.currentTimeMillis();

    initializeComponents();
    JacksonConfig.configureJackson();
    Router router = Router.router(vertx);

    deployFlywayVerticle(vertx).flatMap(migrationVerticleId -> deployApiVerticle(vertx, router))
      .flatMap(apiDeploymentId -> deployHealthVerticle(vertx, router))
      .flatMap(healthDeploymentId -> deployMetricsVerticle(vertx, router))
      .flatMap(metricsDeploymentId -> deployNotesServiceVerticle(vertx, repository))
      .flatMap(serviceDeploymentId -> buildHttpServer(vertx, startPromise, router))
      .onComplete(event -> {
        var diff = System.currentTimeMillis() - start;
        log.info(LogMessage.RUN_DURATION.buildMessage(diff));
      })
      .onFailure(startPromise::fail);
  }

  private void initializeComponents() {
    properties = new ApplicationProperties();
    databaseClient = new DatabaseClient(vertx, properties);
    repository = new NotesRepository(databaseClient.getJdbcPool(), JacksonConfig.getObjectMapper());
    notesValidator = new NotesValidator(vertx);
  }

  private Future<String> deployNotesServiceVerticle(Vertx vertx, NotesRepository repository) {
    return vertx.deployVerticle(new NotesServiceVerticle(repository));
  }

  private Future<String> deployMetricsVerticle(Vertx vertx, Router router) {
    return vertx.deployVerticle(new MetricsVerticle(router));
  }

  private Future<String> deployHealthVerticle(Vertx vertx, Router router) {
    return vertx.deployVerticle(new HealthVerticle(router, databaseClient.getJdbcPool()));
  }

  private Future<String> deployFlywayVerticle(Vertx vertx) {
    return vertx.deployVerticle(new FlywayVerticle(new FlywayConfig(properties)));
  }

  private Future<String> deployApiVerticle(Vertx vertx, Router router) {
    final DeploymentOptions options = new DeploymentOptions().setWorker(true).setWorkerPoolName("api-worker-pool")
      .setInstances(1).setWorkerPoolSize(10);
    return vertx.deployVerticle(new ApiVerticle(router, notesValidator), options);
  }

  private Future<HttpServer> buildHttpServer(Vertx vertx, Promise<Void> promise, Router router) {
    final int port = properties.getServerPort();

    return vertx.createHttpServer().requestHandler(router).listen(port).onSuccess(event -> {
      promise.complete();
      log.info(LogMessage.RUN_HTTP_SERVER_SUCCESS_MESSAGE.buildMessage(port));
    }).onFailure(error -> {
      log.error(String.format("Cannot initialize HTTP server on %s port", port), error);
      promise.fail(error);
    });
  }
}

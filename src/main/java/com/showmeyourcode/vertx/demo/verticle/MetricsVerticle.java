package com.showmeyourcode.vertx.demo.verticle;

import io.micrometer.core.instrument.binder.system.UptimeMetrics;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.VertxOptions;
import io.vertx.ext.web.Router;
import io.vertx.micrometer.MicrometerMetricsOptions;
import io.vertx.micrometer.PrometheusScrapingHandler;
import io.vertx.micrometer.VertxPrometheusOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.constant.RouterPathConstant;
import com.showmeyourcode.vertx.demo.logging.LogMessage;

@Slf4j
@RequiredArgsConstructor
public class MetricsVerticle extends AbstractVerticle {

	private final Router router;

	public static VertxOptions getVertxOptionsWithMetrics() {
		PrometheusMeterRegistry registry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
		new UptimeMetrics().bindTo(registry);

		var metricsOptions = new MicrometerMetricsOptions()
				.setPrometheusOptions(new VertxPrometheusOptions().setEnabled(true)).setJvmMetricsEnabled(true)
				.setMicrometerRegistry(registry).setEnabled(true);
		return new VertxOptions().setMetricsOptions(metricsOptions);
	}

	@Override
	public void start(Promise<Void> promise) {
		log.info(LogMessage.STARTING_MESSAGE.buildMessage(this.getClass().getName()));

		router.get(RouterPathConstant.METRICS).handler(PrometheusScrapingHandler.create());

		promise.complete();
	}
}

package com.showmeyourcode.vertx.demo.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.LoggerFormat;
import io.vertx.ext.web.handler.LoggerHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.config.JacksonConfig;
import com.showmeyourcode.vertx.demo.constant.ApiConstant;
import com.showmeyourcode.vertx.demo.constant.RouterPathConstant;
import com.showmeyourcode.vertx.demo.logging.LogMessage;
import com.showmeyourcode.vertx.demo.model.NoteCreationDTO;
import com.showmeyourcode.vertx.demo.response.ResponseBuilder;
import com.showmeyourcode.vertx.demo.validator.NotesValidator;

@Slf4j
@RequiredArgsConstructor
public class ApiVerticle extends AbstractVerticle {

	private final Router router;
	private final NotesValidator validationHandler;

	@Override
	public void start(Promise<Void> promise) {
		log.info(LogMessage.STARTING_MESSAGE.buildMessage(this.getClass().getName()));

		router.mountSubRouter(RouterPathConstant.V1, notesRouter(router));

		promise.complete();
	}

	/**
	 * All routes are composed by an error handler, a validation handler and the
	 * actual business logic handler
	 */
	private Router notesRouter(Router router) {

		router.route(RouterPathConstant.NOTES_PREFIX + "*").handler(BodyHandler.create());

		router.get(RouterPathConstant.NOTES_PREFIX)
      .handler(LoggerHandler.create(LoggerFormat.DEFAULT))
				.handler(validationHandler.validateGetRequest())
      .handler(this::readAll)
      .failureHandler(routingContext -> {
					ResponseBuilder.buildErrorResponse(routingContext, routingContext.failure());
				});

		router.post(RouterPathConstant.NOTES_PREFIX)
      .handler(LoggerHandler.create(LoggerFormat.DEFAULT))
      .handler(validationHandler.validatePostRequest())
      .handler(this::create)
				.failureHandler(routingContext -> {
					ResponseBuilder.buildErrorResponse(routingContext, routingContext.failure());
				});

		return router;
	}

	public Future<Object> create(RoutingContext routingContext) {
    NoteCreationDTO noteToCreate;
		try {
      noteToCreate = JacksonConfig.getObjectMapper().readValue(routingContext.getBodyAsJson().toString(),
					NoteCreationDTO.class);
		} catch (Exception e) {
			log.error("Cannot parse request's body - note creation. ", e);
			ResponseBuilder.buildErrorResponse(routingContext, e);
			return Future.future(event -> event.fail(e));
		}

    final EventBus eventBus = vertx.eventBus();
    return eventBus.request(NotesServiceVerticle.CREATE_NOTES_ADDRESS, noteToCreate)
      .map(Message::body)
      .onSuccess(success -> {
        ResponseBuilder.buildCreatedJsonResponse(routingContext, success);
      })
      .onFailure(throwable ->
        ResponseBuilder.buildErrorResponse(routingContext, throwable)
      );
	}

	private Future<Object> readAll(RoutingContext rc) {
		final String page = rc.queryParams().get(ApiConstant.PAGE_NUMBER_QUERY_PARAM);
		final String limit = rc.queryParams().get(ApiConstant.LIMIT_QUERY_PARAM);
		log.info("Getting all notes for page: {} and limit: {} ...", page, limit);

    JsonObject entries = new JsonObject();
    entries.put(NotesServiceVerticle.GET_NOTES_PAGE_PARAM, page);
    entries.put(NotesServiceVerticle.GET_NOTES_LIMIT_PARAM, limit);
    entries.put("time", System.currentTimeMillis());

    final EventBus eventBus = vertx.eventBus();
    return eventBus.request(NotesServiceVerticle.GET_NOTES_ADDRESS, entries)
      .map(Message::body)
      .onSuccess(success -> {
        ResponseBuilder.buildOkJsonResponse(rc, success);
      })
      .onFailure(throwable ->
        ResponseBuilder.buildErrorResponse(rc, throwable)
      );
	}
}

package com.showmeyourcode.vertx.demo.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Pool;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.constant.RouterPathConstant;
import com.showmeyourcode.vertx.demo.logging.LogMessage;

@Slf4j
@RequiredArgsConstructor
public class HealthVerticle extends AbstractVerticle {

	private final Router router;
	private final Pool dbClient;

	@Override
	public void start(Promise<Void> promise) {
		log.info(LogMessage.STARTING_MESSAGE.buildMessage(this.getClass().getName()));
		final HealthCheckHandler healthCheckHandler = HealthCheckHandler.create(vertx);

		healthCheckHandler.register("database", promiseDb -> dbClient.getConnection(connection -> {
			if (connection.failed()) {
				log.error("Health check failed! ", connection.cause());
				promiseDb.fail(connection.cause());
			} else {
				connection.result().close();
				promiseDb.complete(Status.OK());
			}
		}));

		router.get(RouterPathConstant.HEALTH).handler(healthCheckHandler);

		promise.complete();
	}
}

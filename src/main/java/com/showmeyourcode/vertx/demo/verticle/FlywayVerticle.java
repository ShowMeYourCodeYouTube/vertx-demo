package com.showmeyourcode.vertx.demo.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.Configuration;
import com.showmeyourcode.vertx.demo.config.FlywayConfig;
import com.showmeyourcode.vertx.demo.logging.LogMessage;

@Slf4j
public class FlywayVerticle extends AbstractVerticle {

	private FlywayConfig config;

	public FlywayVerticle(FlywayConfig config) {
		this.config = config;
	}

	@Override
	public void start(Promise<Void> promise) {
		log.info(LogMessage.STARTING_MESSAGE.buildMessage(this.getClass().getName()));

		final Configuration migrationsConfiguration = config.buildFlywayConfiguration();
		final Flyway flyway = new Flyway(migrationsConfiguration);

		try {
			flyway.migrate();
			promise.complete();
		} catch (Exception e) {
			log.error("Cannot do Flyway migrations! Details: {}", e.getMessage());
			promise.fail(e);
		}
	}
}

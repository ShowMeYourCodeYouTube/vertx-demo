package com.showmeyourcode.vertx.demo.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.constant.ApiConstant;
import com.showmeyourcode.vertx.demo.model.GetAllNotesDTO;
import com.showmeyourcode.vertx.demo.model.NoteDTO;
import com.showmeyourcode.vertx.demo.model.NoteCreationDTO;
import com.showmeyourcode.vertx.demo.repository.NotesRepository;

import java.util.Objects;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class NotesServiceVerticle extends AbstractVerticle {

  public static final String GET_NOTES_ADDRESS = "notes.get";
  public static final String CREATE_NOTES_ADDRESS = "notes.create";
  // An example how to pass parameters
  public static final String GET_NOTES_PAGE_PARAM = "page";
  public static final String GET_NOTES_LIMIT_PARAM = "limit";
  public static final int GENERAL_FAILURE_CODE = 500;

	private final NotesRepository repository;

  @Override
  public void start() throws Exception {
    vertx.eventBus().consumer(GET_NOTES_ADDRESS,this::onGetMessage);
    vertx.eventBus().consumer(CREATE_NOTES_ADDRESS,this::onCreateMessage);
  }

  private <T> void onCreateMessage(Message<T> messageToProcess) {
    NoteCreationDTO message = (NoteCreationDTO) messageToProcess.body();
    create(message)
      .onSuccess(s-> messageToProcess.reply(s))
      .onFailure(err->messageToProcess.fail(GENERAL_FAILURE_CODE, err.getMessage()));
  }

  private <T> void onGetMessage(Message<T> messageToProcess) {
    JsonObject message = (JsonObject) messageToProcess.body();
    readAll(message.getString(GET_NOTES_PAGE_PARAM), message.getString(GET_NOTES_LIMIT_PARAM))
      .onSuccess(s-> messageToProcess.reply(s))
      .onFailure(err->messageToProcess.fail(GENERAL_FAILURE_CODE, err.getMessage()));
  }

	public Future<GetAllNotesDTO> readAll(String pageParam, String limitParam) {
		int page = Objects.isNull(pageParam) ? ApiConstant.PAGINATION_PAGE : Integer.parseInt(pageParam);
		int limit = Objects.isNull(limitParam) ? ApiConstant.PAGINATION_LIMIT : Integer.parseInt(limitParam);
		return CompositeFuture.all(
        repository.selectAll(limit, page, getOffset(page, limit)),
        repository.count()
      ).map(asyncResult -> asyncResult.resultAt(0));
	}

	public Future<NoteDTO> create(NoteCreationDTO dto) {
		return repository.save( dto);
	}

	public static int getOffset(int page, int limit) {
      return page * limit;
	}
}

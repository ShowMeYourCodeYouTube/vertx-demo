package com.showmeyourcode.vertx.demo.constant;

public class RouterPathConstant {

	public static final String NOTES_PREFIX = "/notes";
	public static final String V1 = "/api/v1";
	public static final String HEALTH = "/health";
	public static final String METRICS = "/metrics";
	public static final String V1_NOTES = V1 + NOTES_PREFIX;
}

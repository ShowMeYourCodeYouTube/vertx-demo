package com.showmeyourcode.vertx.demo.constant;

public class ApiConstant {
	public static final int PAGINATION_LIMIT = 10;
	public static final int PAGINATION_PAGE = 0;
	public static final String PAGE_NUMBER_QUERY_PARAM = "pn";
	public static final String LIMIT_QUERY_PARAM = "l";
}

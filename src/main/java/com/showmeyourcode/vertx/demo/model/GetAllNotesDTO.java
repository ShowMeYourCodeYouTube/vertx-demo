package com.showmeyourcode.vertx.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAllNotesDTO implements Serializable {

	public static final long serialVersionUID = 1L;

	private List<NoteDTO> notes = new ArrayList<>();
	private int total;
	private int limit;
	private int page;
}

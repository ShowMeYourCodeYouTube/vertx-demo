package com.showmeyourcode.vertx.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Fields are snake_case as they are directly mapped from database. It's easier
 * to do it this way than writing own mappers.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NoteCreationDTO implements Serializable {

	public static final long serialVersionUID = 1L;

	private String author;
	private String creation_date;
	private String content;
}

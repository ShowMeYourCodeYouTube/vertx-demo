package com.showmeyourcode.vertx.demo.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ErrorResponseDTO implements Serializable {

	public static final long serialVersionUID = 1L;

	private final int status;
	private final String error;
	private final String timestamp;
}

package com.showmeyourcode.vertx.demo.response;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.validation.BodyProcessorException;
import io.vertx.ext.web.validation.ParameterProcessorException;
import lombok.extern.slf4j.Slf4j;
import com.showmeyourcode.vertx.demo.config.JacksonConfig;
import com.showmeyourcode.vertx.demo.model.ErrorResponseDTO;

import java.time.Instant;

@Slf4j
public class ResponseBuilder {

	private static final String APPLICATION_JSON = "application/json";

	private ResponseBuilder() {
	}

	public static void buildOkJsonResponse(RoutingContext routingContext, Object body) {
		routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
				.setStatusCode(HttpResponseStatus.OK.code()).end(Json.encode(body));
	}

	public static void buildCreatedJsonResponse(RoutingContext routingContext, Object body) {
		routingContext.response().putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
				.setStatusCode(HttpResponseStatus.CREATED.code()).end(Json.encode(body));
	}

	/**
	 * Build error response based on throwable type.
	 */
	public static void buildErrorResponse(RoutingContext rc, Throwable throwable) {
		log.error("Building error response for: ", throwable);
		final int status;
		final String message;
		String response = "{}";

		if (throwable instanceof IllegalArgumentException || throwable instanceof IllegalStateException
				|| throwable instanceof BodyProcessorException || throwable instanceof ParameterProcessorException) {
			status = HttpResponseStatus.BAD_REQUEST.code();
			message = throwable.getMessage();
		} else {
			status = HttpResponseStatus.INTERNAL_SERVER_ERROR.code();
			message = "Internal Server Error";
		}

		try {
			response = JacksonConfig.getObjectMapper()
					.writeValueAsString(new ErrorResponseDTO(status, message, Instant.now().toString()));
		} catch (Exception e) {
			log.error("Cannot construct error response! Using default one. ", e);
		}

		rc.response().setStatusCode(status).putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON).end(response);
	}
}

import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.tasks.testing.logging.TestLogEvent.*

plugins {
  java
  application
  id("com.github.johnrengelman.shadow") version "8.1.1"

  id("org.sonarqube") version "4.3.0.3225"

  jacoco
}

group = "com.showmeyourcode.vertx"
version = "1.0.0-SNAPSHOT"

repositories {
  mavenCentral()
}

val vertxVersion = "4.4.4"
val junitJupiterVersion = "5.10.0"
val scramClientVersion = "2.1"
val prometheusVersion = "1.11.3"
val flywayVersion = "9.22.0"
val jdbcClientVersion = "4.4.4"
val lombokVersion = "1.18.28"
val sl4jVersion = "2.0.7"
val logbackVersion = "1.4.11"
val mockitoVersion = "5.5.0"
val agroalVersion = "2.2"
val h2Version = "2.2.220"

val mainVerticleName = "com.showmeyourcode.vertx.demo.MainVerticle"
val launcherClassName = "io.vertx.core.Launcher"

val watchForChange = "src/**/*"
val doOnChange = "${projectDir}/gradlew classes"

application {
  mainClass.set(launcherClassName)
  applicationDefaultJvmArgs = listOf("-Dio.netty.tryReflectionSetAccessible=true", "--add-opens=java.base/java.util=ALL-UNNAMED")
}

dependencies {
  implementation(platform("io.vertx:vertx-stack-depchain:$vertxVersion"))
  implementation("io.vertx:vertx-web")
  implementation("io.vertx:vertx-web-validation")
  implementation("io.vertx:vertx-web-client")
  implementation("com.fasterxml.jackson.core:jackson-databind")
  implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

  implementation("io.vertx:vertx-pg-client")
  implementation("io.vertx:vertx-sql-client-templates")
  implementation("com.ongres.scram:client:$scramClientVersion")
  implementation("org.flywaydb:flyway-core:$flywayVersion")
  implementation("io.vertx:vertx-jdbc-client:$jdbcClientVersion")
  implementation("com.h2database:h2:$h2Version")

  implementation("io.agroal:agroal-api:$agroalVersion")
  implementation("io.agroal:agroal-pool:$agroalVersion")

  implementation("io.vertx:vertx-health-check")
  implementation("io.vertx:vertx-micrometer-metrics")
  implementation("io.micrometer:micrometer-registry-prometheus:$prometheusVersion")

  implementation("io.vertx:vertx-json-schema")

  implementation("org.slf4j:slf4j-simple:$sl4jVersion")
  implementation("ch.qos.logback:logback-classic:$logbackVersion")
  compileOnly("org.projectlombok:lombok:$lombokVersion")
  annotationProcessor("org.projectlombok:lombok:$lombokVersion")

  testImplementation("io.vertx:vertx-unit")
  testImplementation("io.vertx:vertx-junit5")
  testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
  testImplementation("org.junit.jupiter:junit-jupiter-params:$junitJupiterVersion")
  testImplementation("org.mockito:mockito-core:$mockitoVersion")
  testCompileOnly("org.projectlombok:lombok:$lombokVersion")
  testAnnotationProcessor("org.projectlombok:lombok:$lombokVersion")
}

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

sonarqube {
  properties {
    property("sonar.projectKey", "ShowMeYourCodeYouTube_vertx-demo")
    property("sonar.organization", "showmeyourcodeyoutube")
    property("sonar.core.codeCoveragePlugin", "jacoco")
    property("sonar.coverage.jacoco.xmlReportPaths", "${project.buildDir}/reports/jacoco/test/jacocoTestReport.xml")
    property("sonar.coverage.exclusions", "**/demo/VertxApplication*")
  }
}

tasks.withType<ShadowJar> {
  archiveClassifier.set("fat")
  manifest {
    attributes(mapOf("Main-Verticle" to mainVerticleName))
  }
  mergeServiceFiles()
}

jacoco {
  toolVersion = "0.8.10"
}

tasks.test {
  finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
  dependsOn(tasks.test)
}

tasks.jacocoTestReport {
  reports {
    xml.required.set(true)
    csv.required.set(false)
    html.required.set(true)
  }
  classDirectories.setFrom(
    sourceSets.main.get().output.asFileTree.matching {
      exclude("**/demo/VertxApplication*")
    }
  )
}


tasks.withType<Test> {
  useJUnitPlatform()
  testLogging {
    events = setOf(PASSED, SKIPPED, FAILED)
  }
}

tasks.withType<JavaExec> {
  args = listOf("run", mainVerticleName, "--redeploy=$watchForChange", "--launcher-class=$launcherClassName", "--on-redeploy=$doOnChange")
}
